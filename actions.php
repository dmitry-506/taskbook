<?php 

	session_start();
  include "db.php";
  include "functions.php";
	
  //Авторазация
  if (isset($_POST['do_login'])) {

    $url = "login.php";
    $login = check($_POST['login']);
    $password = check($_POST['password']);

    $errors = array();

    if ($login == '') {
      $errors[] = "Введите логин";
    }

    if ($password == '') {
      $errors[] = "Введите пароль";
    }

    $query = $mysqli->query("SELECT * FROM users WHERE username = '$login' LIMIT 1");
    $row = mysqli_fetch_assoc($query);

    if ($row) {

      if (password_verify($password, $row['user_hash'])) {

        $_SESSION['logged_user'] = $row['username'];

        header('Location: /');

      } else {
        $errors[] = "Пользователь с такоим логином или паролем не найден";
      }

    } else {
      $errors[] = "Пользователь с такоим логином или паролем не найден";
    }

    if (!empty($errors)) {

      $_SESSION['message'] = "<div id='message' class=\"alert alert-danger\" role=\"alert\">" . array_shift($errors) . "</div>";
      header("Refresh:0; url='/" . $url . "', true, 303");

    }
  }


  //Отмена
  if (isset($_POST['cancel'])) {
    header('Location: /');
  }


	//Добавление
  if (isset($_POST['do_add'])) {

    $url = "index.php";
    $username = check($_POST['username']);
    $email = check($_POST['email']);
    $task = check($_POST['task']);

    $errors = array();

    if ($username == '') {
    $errors[] = "Не сохранено: Введите имя пользователя";
    }

    if ($email == '') {
    $errors[] = "Не сохранено: Введите E-mail";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors[] = "Не сохранено: E-mail адрес указан неверно";
    }

    if ($task == '') {
    $errors[] = "Не сохранено: Введите текст задачи";
    }

    if (empty($errors)) {

      //поиск уже похожего юзера
      $query = $mysqli->query("SELECT id FROM users WHERE username = '$username' LIMIT 1");
      $row = mysqli_fetch_assoc($query);

      if (!$row) {

        $query = "INSERT INTO users VALUES(null, '$username', '', Now(), '')";
        $mysqli->query($query);

        $query = $mysqli->query("SELECT id FROM users WHERE username = '$username' LIMIT 1");
        $user_id = mysqli_fetch_assoc($query)['id'];

      } else {
        $user_id = $row['id'];
      }

      //вставка в таблицу
      $query = "INSERT INTO tasks VALUES(null, $user_id, Now(), '$email', '$task', 'новая', 0)";
      $mysqli->query($query);

      $_SESSION['message'] = "<div id='message' class=\"alert alert-success\" role=\"alert\"> Успешно добавлено </div>";

    } else {

      $_SESSION['message'] = "<div id='message' class=\"alert alert-danger\" role=\"alert\">" . array_shift($errors) . "</div>";

    }
    header("Refresh:0; url='/'" . $url . ", true, 303"); 
  }


	//Редактирование
  if (isset($_POST['do_save']) && isset($_SESSION['logged_user'])) {

    if (!$_GET) {
      $task_id = $_POST['task_id'];
    } else {
      $task_id = $_GET['id'];
    }

    $query = $mysqli->query("SELECT * FROM tasks INNER JOIN users ON tasks.user_id = users.id WHERE tasks.id = '$task_id' LIMIT 1");
    $row = mysqli_fetch_assoc($query);

    $url = "edit.php";
    $username = check($_POST['username']);
    $email = check($_POST['email']);
    $task = check($_POST['task']);
    $status = check($_POST['status']);
    $task_id = $_POST['task_id'];

    $errors = array();

    if ($username == '') {
      $errors[] = "Не сохранено: Введите имя пользователя";
    }

    if ($email == '') {
      $errors[] = "Не сохранено: Введите E-mail";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors[] = "Не сохранено: E-mail адрес указан неверно";
    }

    if ($task == '') {
      $errors[] = "Не сохранено: Введите текст задачи";
    }

    if ($status == '') {
      $errors[] = "Не сохранено: Выберите статус задачи";
    }

    if (empty($errors)) {

      if ($task != $row['task'] || $row['task'] != 0) {
        $edited = 1;
      } else {
        $edited = 0;
      }

      //поиск уже похожего юзера
      $query = $mysqli->query("SELECT id FROM users WHERE username = '$username' LIMIT 1");
      $row = mysqli_fetch_assoc($query);

      if (!$row) {

        $query = "INSERT INTO users VALUES(null, '$username', '', Now(), '')";
        $mysqli->query($query);

        $query = $mysqli->query("SELECT id FROM users WHERE username = '$username' LIMIT 1");
        $user_id = mysqli_fetch_assoc($query)['id'];

      } else {
        $user_id = $row['id'];
      }

      //обновление записа в базе
      $query = "UPDATE tasks SET user_id = '$user_id', email = '$email', edited = '$edited', task = '$task', status = '$status'  WHERE id = '$task_id' LIMIT 1";
      $mysqli->query($query);

      $_SESSION['message'] = "<div id='message' class=\"alert alert-success\" role=\"alert\">Сохранено</div>";

    } else {
      $_SESSION['message'] = "<div id='message' class=\"alert alert-danger\" role=\"alert\">" . array_shift($errors) . "</div>";
    }

    header("Refresh:0; url='/" . $url . "?id=$task_id', true, 303");

  } else if (isset($_POST['do_save']) && !$_SESSION['logged_user']) {
    header('Location: /login.php');
  }

  $mysqli->close();

 ?>
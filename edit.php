<?php 
  
  $url = "edit.php";

  if (!$_GET) {
    $task_id = $_POST['task_id'];
  } else {
    $task_id = $_GET['id'];
  }

  include "db.php";

  $query = $mysqli->query("SELECT * FROM tasks INNER JOIN users ON tasks.user_id = users.id WHERE tasks.id = '$task_id' LIMIT 1");
  $row = mysqli_fetch_assoc($query);

  $mysqli->close();

  include "actions.php";
  include "header-footer.php";

  if (!$_SESSION['logged_user']) {
    header('Location: /login.php');
  }

  echo $header;
  
?>

<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">

        <div class="col-lg-6 col-sm-12"><h3><?php echo $titles[$url]; ?></h3></div>
        <div class="col-lg-6 col-sm-12">
          <h3 class="pull-right">

            <?php 

              if (isset($_SESSION['logged_user'])) {
                echo "Авторизован: " . $_SESSION['logged_user'] . " | <a href=\"/logout.php\">Выйти</a>";
              } 

            ?>

          </h3>
        </div>
      </div>

      <hr>
      <div class="col-lg-12">
        
        <form name="form" method="POST" action="/actions.php">
          <input type="hidden" name="task_id" value="<?php echo $task_id; ?>">
          
          <div class="form-group col-lg-4 col-sm-12">
            <input id="username" type="text" name="username" placeholder="Имя пользователя" class="form-control input-lg" value="<?php echo $row['username']; ?>">
          </div>
          
          <div class="form-group col-lg-4 col-sm-12">
            <input  id="email" type="text" name="email" placeholder="E-mail" class="form-control input-lg" value="<?php echo $row['email']; ?>">
          </div>

          <div class="form-group col-lg-4 col-sm-12">
            <select name="status" class="form-control my-select">
              <option value="">Выберите...</option>
              <option <?php if($row['status'] == "новая") {echo " selected";} ?> value="новая">Новая</option>
              <option <?php if($row['status'] == "выполнено") {echo " selected";} ?> value="выполнено">Выполнено</option>
            </select>
          </div>

          <div class="form-group col-lg-12">
            <textarea name="task" placeholder="Текст задачи" class="form-control" rows="3"><?php echo $row['task']; ?></textarea>
          </div>

          <div class="form-group col-lg-8">

            <?php 

              if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
              }

            ?>

          </div>

          <div class="form-group col-lg-2">
            <button name="do_save" class="btn btn-lg btn-width btn-success pull-right">Сохранить</button>
          </div>

          <div class="form-group col-lg-2">
            <button name="cancel" class="btn btn-lg btn-width btn-info pull-right">Вернуться</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<?php echo $footer; ?>
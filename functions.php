<?php

	function check($val){
		$check = false;
		$check = htmlspecialchars(trim($val), ENT_QUOTES, 'UTF-8');
		return $check;
	}

	function pageLinks($page, $pagesCount, $order, $sort){

		$pagination = false;

		$prev = $page - 1;

		$pagination .= "<nav aria-label=\"Page navigation\"><ul class=\"pagination\">";

		if ($pagesCount > 0) {
			if ($page == 1) {

				$pagination .= "<li class=\"page-item\" disabled>

				<span aria-hidden=\"true\">&laquo;</span></li>";

			} else {

				$pagination .= "<li class=\"page-item\">
				<a class=\"page-link\" href=\"?order=$order&&page=$prev&&sort=$sort&&revers=0\" aria-label=\"Previous\">
				<span aria-hidden=\"true\">&laquo;</span>
				</a>
				</li>";

			}


			for ($i=1; $i <= $pagesCount; $i++) {

				if ($page == $i) {
					$class = " active";
				}else{
					$class = "";
				}

				$pagination .= "<li class=\"page-item$class\"><a class=\"page-link\" href=\"?order=$order&&page=$i&&sort=$sort&&revers=0\">$i</a></li>";

			}

			$next = $page + 1;

			if ($page == $pagesCount) {

				$pagination .= "<li class=\"page-item\" disabled>
				<span aria-hidden=\"true\">&raquo;</span></li>";

			} else {
				$pagination .= "<li class=\"page-item\">
				<a class=\"page-link\" href=\"?order=$order&&page=$next&&sort=$sort&&revers=0\" aria-label=\"Next\">
				<span aria-hidden=\"true\">&raquo;</span>
				</a>
				</li>";
			}
		}

		$pagination .= "</ul></nav>";

		return $pagination;

	}

 ?>
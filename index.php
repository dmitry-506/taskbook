<?php 

	$url = "index.php";
	include "actions.php";

	//Пагинация
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	}else{
		$page = 1;
	}

	$tasksOnPage = 3;

	$from = ($page - 1) * $tasksOnPage;

	include "db.php";

	$query2 = $mysqli->query("SELECT COUNT(*) as count FROM tasks");
	$count = mysqli_fetch_assoc($query2)['count'];

	$pagesCount = ceil($count / $tasksOnPage);


	//Сортировка
	if (isset($_GET['order'])) {
		$order = $_GET['order'];
	} else {
		$order = 'tasks.id';
	}

	if (!$_GET) {
		$sort = 'ASC';
		$_GET['revers'] = 0;
	}

	if (isset($_GET['sort'])) {
		$sort = $_GET['sort'];
	}

	if ($_GET['revers'] == 1) {
		$sort == 'DESC' ? $sort = 'ASC' : $sort = 'DESC';
	} 

	//Список задач
	$query = $mysqli->query("SELECT *, tasks.id as task_id FROM tasks INNER JOIN users ON tasks.user_id = users.id ORDER BY $order $sort LIMIT $from,$tasksOnPage");

	$mysqli->close();

	include "header-footer.php";
	echo $header;

 ?>

	<div class="wrapper">
      <div class="container">
        <div class="row">

			<div class="col-lg-12">
				
				<div class="col-lg-6 col-sm-12"><h3>Новая задача</h3></div>
				<div class="col-lg-6 col-sm-12"><h3 class="pull-right">
					
					<?php 

						if (isset($_SESSION['logged_user'])) {
							echo "Авторизован: " . $_SESSION['logged_user'] . " | <a href=\"/logout.php\">Выйти</a>";
						} else {
							echo "<a href=\"/login.php\">Войти</a><br>";
						}

					?>

				</h3></div>
			</div>

			<div class="col-lg-12">
			    <form id="myform" name="form" method="POST" action="/actions.php">
			      <div class="form-group col-lg-6 col-sm-12">
			        <input id="username" type="text" name="username" placeholder="Имя пользователя" class="form-control input-lg">
			      </div>
			      <div class="form-group col-lg-6 col-sm-12">
			        <input  id="email" type="text" name="email" placeholder="E-mail" class="form-control input-lg">
			      </div>
			      <div class="form-group col-lg-12">
			      	<textarea  id="task" name="task" placeholder="Текст задачи" class="form-control" rows="3"></textarea>
			      </div>

  						<div class="form-group col-lg-9">
	  						<?php 

	  							if (isset($_SESSION['message'])) {
	  								echo $_SESSION['message'];
	  								unset($_SESSION['message']);
	  							}

	  						 ?>
	  					</div>

					  <div class="form-group col-lg-3">
				  		<button id="do_add" name="do_add" class="btn btn-lg btn-width btn-success pull-right">Добавить</button>
	  				  </div>

			    </form>
			</div>
			
			<div class="col-lg-12">
				
				<h3><?php echo $titles[$url]; ?></h3>
				<hr>
				
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="col-lg-2"><a href="?order=username&&sort=<?php echo $sort; ?>&&revers=1">Имя пользователя</a></th>
							<th class="col-lg-2"><a href="?order=email&&sort=<?php echo $sort; ?>&&revers=1">Email</a></th>
							<th class="col-lg-6">Текст задачи</th>
							<th class="col-lg-2"><a href="?order=status&&sort=<?php echo $sort; ?>&&revers=1">Статус</a></th>
						</tr>
					</thead>
					<tbody>
						<?php 

							while ($row = mysqli_fetch_assoc($query)) {
								
								echo "
									<tr>
										<td>" . $row['username'] . "</td>
										<td>" . $row['email'] . "</td>
										<td>" . $row['task'];

											if ($row['edited'] == 1) {
												echo "<br><span class=\"edited\">(отредактировано администратором)</span>";
											}

										 echo "</td>
										<td>";

											if ((isset($_SESSION['logged_user'])) == 'admin') {
												echo " <a href=\"edit.php?id=" . $row['task_id'] . "\">ред.</a> ";
											}

										 echo $row['status'] . "</td>
									</tr>
								";
							}
						 ?>
					</tbody>
				</table>

				<?php echo pageLinks($page, $pagesCount, $order, $sort); ?>

			</div>
        </div>
 	  </div>
    </div>

<?php echo $footer; ?>
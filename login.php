<?php 

  $url = "login.php";
  include "actions.php";
  include "header-footer.php";
  echo $header;

?>

<div class="wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 center">
        
        <h3 class="center-horizontal"><?php echo $titles[$url]; ?></h3>
        <hr>

        <form method="POST" action="/actions.php">
          
          <div class="form-group col-lg-12">
            <input type="text" name="login" placeholder="Логин" class="form-control input-lg">
          </div>
          <div class="form-group col-lg-12">
            <input type="password" name="password" placeholder="Пароль" class="form-control input-lg">
          </div>

          <div class="col-lg-6">
            <button name="do_login" class="btn btn-lg btn-success btn-width">Войти</button>
          </div>

          <div class="col-lg-6">
            <button name="cancel" class="btn btn-lg btn-info btn-width">Отмена</button>
          </div>

          <div class="clear col-lg-12"><hr class="my-hr"></div>

          <div class="form-group col-lg-12">

            <?php 

              if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
              }

            ?>

          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<?php echo $footer; ?>
<?php 
	
	session_start();
	require "db.php";
	
	$mysqli->close();
	unset($_SESSION['logged_user']);
	unset($_SESSION['message']);
	header('Location: /');

 ?>